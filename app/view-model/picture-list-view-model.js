var observableArrayModule = require("data/observable-array");

function PictureListViewModel(items) {
    var viewModel = new observableArrayModule.ObservableArray(items);

    viewModel.empty = function() {
        while (viewModel.length) {
            viewModel.pop();
        }
    };

    viewModel.add = function(picture) {
      if(picture === undefined || (picture.src=="" && picture.name=="")){
          return;
      }
        viewModel.push({ name: picture.name, src: picture.src });
    };

    viewModel.delete = function(index) {
        viewModel.splice(index, 1);
    };

    return viewModel;
}

module.exports = PictureListViewModel;