var observableModule = require("data/observable");

function Picture(data) {
    data = data || {};

    var viewModel = new observableModule.Observable({
        src: data.src || "",
        name: data.name || ""
    });

    return viewModel;
}

module.exports = Picture;