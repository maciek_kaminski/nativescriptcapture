var frameModule = require("ui/frame");
var viewModule = require("ui/core/view");
var camera = require("camera");
var Picture = require("../view-model/picture-view-model");

var picture;

exports.loaded = function(args) {
    page = args.object;
    if(!picture){
       picture = new Picture();
       picture.set("src","https://placehold.it/200x200");
    }
    pictureListCallback = page.navigationContext;
    page.bindingContext = picture;

};

exports.takePicture = function() {
    camera.takePicture().then(function (result) {
        picture.set("src" , result);
        picture.set("alreadyTaken", true);
    });
};

exports.addPicture = function() {
    pictureListCallback(picture);

    picture = new Picture();
    picture.set("src","https://placehold.it/200x200");
    frameModule.topmost().goBack();
};