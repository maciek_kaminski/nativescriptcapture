var camera = require("camera");
var frameModule = require("ui/frame");
var observableModule = require("data/observable");
var PictureListViewModel = require("./view-model/picture-list-view-model");
var swipeDelete = require("./utils/ios-swipe-delete");
var viewModule = require("ui/core/view");
var dialogsModule = require("ui/dialogs");
var gestures = require("ui/gestures");


var pictureList = new PictureListViewModel([]);
var pageData = new observableModule.Observable({
    pictureList: pictureList
});

function pageLoaded(args) {
    page = args.object;
    page.bindingContext = pageData;

    if (page.ios) {
        var navigationBar = frameModule.topmost().ios.controller.navigationBar;
        navigationBar.barStyle = UIBarStyle.UIBarStyleBlack;
    }

    if (page.ios) {
     var listView = viewModule.getViewById(page, "pictureList");
        swipeDelete.enable(listView, function(index) {
            pictureList.delete(index);
        });
    }

    var takePictureBtn = viewModule.getViewById(page, "takePictureBtn");
    var observer = takePictureBtn.on("longPress", showOSVersion);
}


function takePicture() {
    frameModule.topmost().navigate({
            moduleName: "view/take-picture",
            context: function(picture){
                if (page.android) {
                    var time = new android.text.format.Time();
                    time.setToNow();
                    picture.set("name",picture.name + " - "+time.format("%D"))
                }
                pictureList.add(picture);
            }
        });
}

function showOSVersion(){
    var version = "??";
    if (page.ios) {
     version = UIDevice.currentDevice().systemVersion;
    }

    if (page.android) {
     version = android.os.Build.VERSION.RELEASE;
    }

    console.log(version);
    dialogsModule.alert({
         message: "System version is: "+version,
         okButtonText: "OK"
     });
}

function deletePic(args) {
    var item = args.view.bindingContext;
    var index = pictureList.indexOf(item);
    pictureList.delete(index);
};

exports.pageLoaded = pageLoaded;
exports.takePicture = takePicture;
exports.deletePic = deletePic;
